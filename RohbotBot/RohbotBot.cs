﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using WebSocketSharp;
using Newtonsoft.Json;

namespace RohbotBot
{

    public class RohbotBot
    {
        string URL;
        string Username;
        string Password;
        public string Owner { get; private set; }
        WebSocket Socket;

        public event EventHandler<MessageEventArgs> OnMessage;

        public RohbotBot(string url, string username, string password, string owner)
        {
            URL = url;
            Username = username;
            Password = password;
            Owner = owner;
        }

        public void Connect()
        {
            Socket = new WebSocket(URL);
            Console.WriteLine("Connecting");
            Socket.EnableRedirection = true;
            Socket.Connect();
            Console.WriteLine("Authorizing...");
            Socket.Send("{\"Type\":\"auth\",\"Method\":\"login\",\"Username\":\"" + Username + "\",\"Password\":\"" + Password + "\", \"Tokens\": null}");

            Socket.OnMessage += HandleMessage;
        }

        void HandleMessage(object sender, WebSocketSharp.MessageEventArgs e)
        {
            dynamic data = JsonConvert.DeserializeObject(e.Data);
            if (data.Type.Value == "message")
            {
                try {
                    OnMessage(this, new MessageEventArgs(data.Line.Sender.Value, data.Line.Chat.Value, WebUtility.HtmlDecode(data.Line.Content.Value)));
                } catch(Exception ex)
                {
                    throw new NotImplementedException();
                }
            }

        }

        public void Connect(string room)
        {
            Connect();
            Join(room);
        }

        public void Connect(string[] rooms)
        {
            Connect();
            Join(rooms);
        }

        public void Join(string room)
        {
            if (Socket.IsAlive)
            {
                Socket.Send("{\"Type\":\"sendMessage\",\"Target\":\"home\",\"Content\":\" /join " + room + "\"}");
            }
        }

        public void Join(string[] rooms)
        {
            if (Socket.IsAlive)
            {
                foreach (var room in rooms)
                {
                    Join(room);
                }
            }
        }

        public void Leave(string room)
        {
            if (Socket.IsAlive)
            {
                Socket.Send("{\"Type\":\"sendMessage\",\"Target\":\"home\",\"Content\":\" /leave " + room + "\"}");
            }
        }

        public void Leave(string[] rooms)
        {
            if (Socket.IsAlive)
            {
                foreach (var room in rooms)
                {
                    Leave(room);
                }
            }
        }

        public void Send(string room, string message)
        {
            Socket.Send("{\"Type\":\"sendMessage\",\"Target\":\"" + room + "\",\"Content\":\"" + message + "\"}");
        }
    }
}