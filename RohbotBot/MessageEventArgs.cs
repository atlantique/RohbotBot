﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RohbotBot
{
    public class MessageEventArgs : EventArgs
    {
        public string Sender { get; private set; }
        public string Room { get; private set; }
        public string Message { get; private set; }

        public MessageEventArgs(string sender, string room, string message)
        {
            Sender = sender;
            Room = room;
            Message = message;
        }
    }
}
